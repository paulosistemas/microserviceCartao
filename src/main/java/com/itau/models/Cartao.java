package com.itau.models;


import com.itau.enums.SituacaoCartao;

import javax.persistence.*;

@Entity
public class Cartao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idCartao;

    private String numeroCartao;

    private SituacaoCartao situacaoCartao;

    private int idCliente;

    public int getIdCartao() {
        return idCartao;
    }

    public void setSituacaoCartao(SituacaoCartao situacaoCartao) {
        this.situacaoCartao = situacaoCartao;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public void setIdCartao(int idCartao) {
        this.idCartao = idCartao;
    }

    public String getNumeroCartao() {
        return numeroCartao;
    }

    public void setNumeroCartao(String numeroCartao) {
        this.numeroCartao = numeroCartao;
    }

    public SituacaoCartao getSituacaoCartao() {
        return situacaoCartao;
    }

}