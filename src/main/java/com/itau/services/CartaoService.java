package com.itau.services;

import com.itau.clients.ClienteClient;
import com.itau.enums.SituacaoCartao;
import com.itau.exceptions.ClienteNotFoundException;
import com.itau.models.Cartao;
import com.itau.repositories.CartaoRepository;
import feign.FeignException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    ClienteClient cliente;

    public Cartao salvarCartao(Cartao cartao){

        cartao.setSituacaoCartao(SituacaoCartao.Inativo);

        validarCliente(cartao.getIdCliente());

        return cartaoRepository.save(cartao);
    }

    public void validarCliente(int idCliente){
        try {
            cliente.getClienteById(idCliente);
        }catch (FeignException.BadRequest e){
            throw new ClienteNotFoundException();
        }
    }

    public Cartao buscarCartaoPorId(int idCartao) {
        Optional<Cartao> optionalCartao = cartaoRepository.findById(idCartao);
        if (optionalCartao.isPresent()) {
            return optionalCartao.get();
        }
        throw new RuntimeException("Cartao não encontrado, ID informado:" + idCartao);
    }

    public Cartao ativarCartao(int idCartao){
        Cartao cartao = buscarCartaoPorId(idCartao);
        cartao.setSituacaoCartao(SituacaoCartao.Ativo);
        cartaoRepository.save(cartao);
        return cartao;
    }

}
