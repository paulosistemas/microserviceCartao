package com.itau.enums;

public enum SituacaoCartao {
    Ativo,
    Inativo
}
