package com.itau.controllers;

import com.itau.clients.ClienteClient;
import com.itau.models.Cartao;
import com.itau.services.CartaoService;
import com.netflix.discovery.converters.Auto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;

    @Autowired
    ClienteClient cliente;


//    {
//        "numeroCartao": "012730871",
//            "idCliente": 2
//    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cartao registrarCartao(@RequestBody @Valid Cartao cartao){



        return cartaoService.salvarCartao(cartao);
    }

    @GetMapping("/{idCartao}")
    @ResponseStatus(HttpStatus.OK)
    public Cartao buscarCartaoPorId(@PathVariable(name="idCartao") int id){
        try{
            return cartaoService.buscarCartaoPorId(id);
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }

    }

    @PatchMapping("/{idCartao}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Cartao ativarCartao(@PathVariable(name="idCartao") int id){
        try{
            return cartaoService.ativarCartao(id);
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

}
