package com.itau.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Cliente proprietario do cartao nao foi encontrado.")
public class ClienteNotFoundException extends RuntimeException{
}
